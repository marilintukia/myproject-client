
async function login(credentials) {
    let response = await fetch(
        `${API_URL}/users/login`,
        {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(credentials)
        }
    );
    return await handleJsonResponse(response);
}
async function register(credentials) {
    let response = await fetch(
        `${API_URL}/users/register`,
        {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(credentials)
        }
    );
    return await handleJsonResponse(response);
}


async function postChild(child) {
    let response = await fetch(
        `${API_URL}/child`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(child)
        }
    );
    handleResponse(response);
}

async function putChild(child) {
    let response = await fetch(
        `${API_URL}/child`,
        {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(child)
        }
    );
    handleResponse(response);
}
async function postSkill(child_id, skillgroup_id) {
    let response = await fetch(
        `${API_URL}/child/skill/${child_id}/${skillgroup_id}`,
        {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(child_id, skillgroup_id)
        }
    );
    handleResponse(response);
}

async function postExercise(child_id, exercise_id) {
    let response = await fetch(
        `${API_URL}/child/exercisecount/${child_id}/${exercise_id}`,
        {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(child_id, exercise_id)
        }
    );
    handleResponse(response);
}


// async function putSkill(child_id, exercise_id) {
//     let response = await fetch(
//         `${API_URL}/child/${child_id}/${exercise_id}`,
//         {
//             method: 'PUT',
//             headers: {
//                 'Content-Type': 'application/json',
//                 'Authorization': `Bearer ${getToken()}`
//             },
//             body: JSON.stringify(child_id, exercise_id)
//         }
//     );
//     handleResponse(response);
// }


async function deleteSkill(child_id, skillgroup_id) {
    let response = await fetch(
        `${API_URL}/child/skill/${child_id}/${skillgroup_id}`,
        {
            method: 'DELETE',
            headers: { 'Authorization': `Bearer ${getToken()}` }
        }
    );
    handleResponse(response);
}

async function fetchSkillGroups() {
    let response = await fetch(
        `${API_URL}/child/skillgroups`,
        {
            method: 'GET'
        }
    );
    return await handleJsonResponse(response);
}

async function fetchAllChildren() {
    let response = await fetch(
        `${API_URL}/child`,
        {
            method: 'GET',
            headers: { 'Authorization': `Bearer ${getToken()}` },
        }
    );
    return await handleJsonResponse(response);
}

async function fetchAllExercises(age_in_months) {
    let response = await fetch(
        `${API_URL}/child/exercises/${age_in_months}`,
        {
            method: 'GET'
        }
    );
    return await handleJsonResponse(response);
}

// async function fetchAllExerciseDescriptions() {
//     let response = await fetch(
//         `${API_URL}/child/exerciseDescription`,
//         {
//             method: 'GET'
//         }
//     );
//     return await handleJsonResponse(response);
// }

async function deleteChild(id) {
    let response = await fetch(
        `${API_URL}/child/${id}`,
        {
            method: 'DELETE',
            headers: { 'Authorization': `Bearer ${getToken()}` }
        }
    );
    handleResponse(response);
}


async function fetchCompany(id) {
    let response = await fetch(
        `${API_URL}/companies/${id}`,
        {
            method: 'GET',
            headers: { 'Authorization': `Bearer ${getToken()}` }
        }
    );
    return await handleJsonResponse(response);
}


async function postFile(file) {
    let formData = new FormData();
    formData.append("file", file);

    let response = await fetch(
        `${API_URL}/files/upload`,
        {
            method: 'POST',
            headers: { 'Authorization': `Bearer ${getToken()}` },
            body: formData
        }
    );
    return await handleJsonResponse(response);
}

// postSkill (lapse id, skill grupi id)

// delete skill (lapse id, skill grupi id)