
// Response and error handlers

function handleResponse(response) {
    if (response.status >= 401 && response.status < 500) {
        clearAuthentication();
        throw new Error('Unauthorized');
    }
}

async function handleJsonResponse(response) {
    if (response.status >= 401 && response.status < 500) {
        clearAuthentication();
        throw new Error('Unauthorized');
    }
    return await response.json();
}

function handleError(e, errorCallback) {
    console.log('Error occurred:', e);
    if (e.message == 'Unauthorized') {
        errorCallback();
    }
}

// Financial functions

function formatNumber(num) {
    if (!isDecimal(num)) {
        return 0;
    }
    num = Math.round(num * 100) / 100;
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

// Validation functions

function isInteger(text) {
    if (text == null) {
        return false;
    }
    let regex = /^[\-]?\d+$/;
    return regex.test(text);
}

function isDecimal(text) {
    if (text == null) {
        return false;
    }
    let regex = /^[\-]?\d+(\.\d+)?$/;
    return regex.test(text);
}

function isEmpty(text) {
    return (!text || 0 === text.length);
}