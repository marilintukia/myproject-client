
let ageInMills = 0;
let ageInDays = 0;
let ageInWeeks = 0;
let ageInWeeksPremature = 0;
// document.location.hash = "#page1"
// Initialization after the HTML document has been loaded...
window.addEventListener('DOMContentLoaded', () => {
    doLoadChildren();
    doLoadSkillGroups();
    displayAllChildrenItems2();
});

/* 
    --------------------------------------------
    ACTION FUNCTIONS
    --------------------------------------------
*/
let skillGroupItems = [];
async function doPostExerciseCount(childId, exerciseId) {
    exerciseCount = await postExercise(childId, exerciseId);
    await doLoadChildren();
    doLoadExercises(childId);
    console.log(exerciseCount);
}

async function doLoadSkillGroups() {
    skillGroupItems = await fetchSkillGroups();
    console.log(skillGroupItems);
}

let allChildrenItems = [];
let activeChildId = 0;

async function doLoadChildren() {
    try {
    allChildrenItems = await fetchAllChildren();
    console.log(allChildrenItems);
    displayAllChildrenItems(allChildrenItems);
    } catch (e)  {
        handleError(e, () => document.location = "./loginindex.html");
    }
}

function getExercisesToDisplay(childSkills, allExercises) {
    let filteredExercises = allExercises;
    console.log('A', filteredExercises.length);
    for (let j = 0; j < childSkills.length; j++) {
        for (let k = 0; k < childSkills[j].skills.length; k++) {
            let childSkillsId = childSkills[j].skills[k].id;
            filteredExercises = filteredExercises.filter(ae => ae.skills_id != childSkillsId);

        }
    }
    console.log('B', filteredExercises.length);
    let groupedExercises = new Object();

    for (let k = 0; k < filteredExercises.length; k++) {
        let currentExerciseSkillgroupId = filteredExercises[k].skillgroup_id;
        if (!groupedExercises[currentExerciseSkillgroupId]) {
            groupedExercises[currentExerciseSkillgroupId] = [];
        }
        groupedExercises[currentExerciseSkillgroupId].push(filteredExercises[k]);
    }
    console.log("grouped", groupedExercises);
    return groupedExercises;
}

let allExercises = [];
async function doLoadExercises(childId) {
    let child = allChildrenItems.find(a => a.id == childId);
    allExercises = await fetchAllExercises(parseInt(Math.ceil(child.prematurityAgeInWeeks / 4)));
    openExercisesModal();
    console.log("All exercises", allExercises);
    console.log("Child skills", child.childSkills);
    let exerciseToDisplay = getExercisesToDisplay(child.childSkills, allExercises);
    let skillGroups = Object.getOwnPropertyNames(exerciseToDisplay);
    console.log(skillGroups);
    let addExercisesHtml = ``;
    console.log(exerciseToDisplay);
    for (let i = 0; i < skillGroups.length; i++) {
        let currentSkillGroup = skillGroups[i];
        console.log(currentSkillGroup);
        let currentExercises = exerciseToDisplay[currentSkillGroup];
        console.log(currentExercises);
        let skillGroupHtml = "";
        for (let j = 0; j < currentExercises.length; j++) {
            let currentExerciseCountObj = child.exerciseCount.find(c => c.exercise_id == currentExercises[j].id);
            let currentExerciseCount = currentExerciseCountObj ? currentExerciseCountObj.count : 0;
            skillGroupHtml = skillGroupHtml + `<br><div><i class="fa fa-star-o" aria-hidden="true"> </i> 
            <a class="link2" onclick="displayExercisePictures(${currentExercises[j].id})" href="#page1" >${currentExercises[j].exerciseName}</a><br>
            <button class="button2" id="clickme" onclick="doPostExerciseCount(${child.id}, ${currentExercises[j].id})">Sooritatud</button></div>`;

        }
        addExercisesHtml = addExercisesHtml + `<div id="skillContainer"style="background: rgb(255, 250, 250)">${skillGroupHtml}<br><button class="button3" onclick="doSaveSkill(${child.id}, ${currentSkillGroup})">Oskus omandatud</button><br></div>`;
    }
    document.querySelector('#addExercises').innerHTML = addExercisesHtml;
}

async function displayExercisesbyGroup() {
    let group1 = await fetchAllExercises(2);
    console.log(group1);
    let group2 = await fetchAllExercises(5);
    let group3 = await fetchAllExercises(8);
    let group4 = await fetchAllExercises(11);
    let allExDisplayHtml = "";
    allExDisplayHtml = allExDisplayHtml + `<div class="row"><div class="col-12"><h2>Esimene vanusegrupp: </h2></div>`;
    for (let i = 0; i < group1.length; i++) {
        console.log(group1[i].exerciseName);
        // allExDisplayHtml = allExDisplayHtml + `<div>${group1[i].exerciseName}</div>`
        for (let j = 0; j < group1[i].exerciseDescriptions.length; j++) {
            console.log(group1[i].exerciseDescriptions[0].pictureName);
            // <div><img class="exPicture" src="${API_URL}/pictures/${group1[i].exerciseDescriptions[j].pictureName}.jpg "></div>
        }
        allExDisplayHtml = allExDisplayHtml + ` <div class="col-3">${group1[i].exerciseName}<img class="exPicture" src="${API_URL}/pictures/${group1[i].exerciseDescriptions[0].pictureName}.jpg"</div></div>`
    }
    allExDisplayHtml = allExDisplayHtml + `<br><div class="col-12"><h2>Teine vanusegrupp: </h2></div>`;
    for (let i = 0; i < group2.length; i++) {
        console.log(group2[i].exerciseName);
        for (let j = 0; j < group2[i].exerciseDescriptions.length; j++) {
            console.log(group2[i].exerciseDescriptions[0].pictureName);
        }
        allExDisplayHtml = allExDisplayHtml + ` <div class="col-3">${group2[i].exerciseName}<img class="exPicture" src="${API_URL}/pictures/${group2[i].exerciseDescriptions[0].pictureName}.jpg"</div></div>`

    }
    allExDisplayHtml = allExDisplayHtml + `<br><div class="col-12"><h2>Kolmas vanusegrupp: </h2></div>`;
    for (let i = 0; i < group3.length; i++) {
        console.log(group3[i].exerciseName);
        for (let j = 0; j < group3[i].exerciseDescriptions.length; j++) {
            console.log(group3[i].exerciseDescriptions[0].pictureName);
        }
        allExDisplayHtml = allExDisplayHtml + ` <div class="col-3">${group3[i].exerciseName}<img class="exPicture" src="${API_URL}/pictures/${group3[i].exerciseDescriptions[0].pictureName}.jpg"</div></div>`
    }
    allExDisplayHtml = allExDisplayHtml + `<br><div class="col-12"><h2>Neljas vanusegrupp: </h2></div>`;
    for (let i = 0; i < group4.length; i++) {
        console.log(group4[i].exerciseName);
        for (let j = 0; j < group4[i].exerciseDescriptions.length; j++) {
            console.log(group4[i].exerciseDescriptions[0].pictureName);
        }
        allExDisplayHtml = allExDisplayHtml + ` <div class="col-3">${group4[i].exerciseName}<img class="exPicture" src="${API_URL}/pictures/${group4[i].exerciseDescriptions[0].pictureName}.jpg"</div></div>`
    }



    document.querySelector('#allChildrenExercises').innerHTML = allExDisplayHtml;
}

function exerciseCompleted(childId, exerciseId) {
    let button = document.getElementById("clickme"),
        count = 0;
    button.onclick = function () {
        count += 1;
        button.innerHTML = "Sooritatud: " + count;
        doLoadExercises();
    }
}

function displayExercisePictures(exId) {
    let currentExercise = allExercises.find(a => a.id == exId);
    console.log(currentExercise);
    let labelHtml = `<div>${currentExercise.exerciseName}</div>`;
    let picturesHtml = ``;
    for (let m = 0; m < currentExercise.exerciseDescriptions.length; m++) {
        picturesHtml = picturesHtml + `
        <div class="exPilt"><img class="exPicture" src="${API_URL}/pictures/${currentExercise.exerciseDescriptions[m].pictureName}.jpg "></div>
        <div class="exDescription">${currentExercise.exerciseDescriptions[m].pictureDescription}</div>
    
        `;

    }
    document.querySelector('#picturesDescriptionContent').innerHTML = picturesHtml;
    document.querySelector('#picturesDescriptionModalLabel').innerHTML = labelHtml;
    $("#picturesDescriptionModal").modal("show");
}


function openChildEditModal(childId) {
    activeChildId = childId;
    $("#addChildModal").modal("show");
    let activeChild = allChildrenItems.find(i => i.id == activeChildId);
    document.querySelector('#firstName').value = activeChild.firstName;
    document.querySelector('#lastName').value = activeChild.lastName;
    document.querySelector('#birthAgeInput').value = activeChild.age;
    document.querySelector('#preMatureWeeks').value = activeChild.prematurity;
}

async function doSaveChild() {
    try {
        let file = document.querySelector('#childPicture').files[0];
        let child = {
            firstName: document.querySelector('#firstName').value,
            lastName: document.querySelector('#lastName').value,
            picture: "",
            age: document.querySelector('#birthAgeInput').value,
            prematurity: document.querySelector('#preMatureWeeks').value,
            trueAgeInWeeks: ageInWeeksPremature,
        };
        if (validateChild(child)) {
            if (document.querySelector('#childPicture').files.length > 0) {
                let fileInfo = await postFile(file);
                child.picture = fileInfo.url;
            }
            if (activeChildId > 0) {
                child.id = activeChildId;
                await putChild(child);
                activeChildId = 0;
            } else {
                await postChild(child);
            }
            doLoadChildren();
            $("#addChildModal").modal("hide");
        }
    } catch (e) {
        // handleError(e, displayLoginPopup);
        console.log(e);
    }
}



async function doSaveSkill(child_id, skillgroup_id) {
    try {
        await postSkill(child_id, skillgroup_id);
        await doLoadChildren();
        displaySkillsModalContent(child_id);
        // doLoadExercises(child_id);
    } catch (e) {
        console.log(e);
    }
}

async function doRemoveSkill(child_id, skillgroup_id) {
    try {
        await deleteSkill(child_id, skillgroup_id);
        await doLoadChildren();
        displaySkillsModalContent(child_id);
    } catch (e) {
        console.log(e);
    }
}

/* 
    --------------------------------------------
    DISPLAY FUNCTIONS
    --------------------------------------------
*/

function calculateWeeks() {
    let birthDateStr = document.querySelector('#birthAgeInput').value;
    if (birthDateStr != "") {
        let birthTime = new Date(birthDateStr).getTime();
        let currentTime = new Date().getTime();
        ageInMills = currentTime - birthTime;
        ageInDays = Math.round(ageInMills / (1000 * 60 * 60 * 24));
        ageInWeeks = Math.round(ageInDays / 7);
        ageInMonths = Math.round(ageInDays / 30.416);
        let prematureInWeeks = parseInt(document.querySelector('#preMatureWeeks').value);
        ageInWeeksPremature = ageInWeeks - prematureInWeeks;
        document.querySelector('#ageInWeeks').innerText = ageInWeeksPremature;
    }
}

function openAddChildForm() {
    openPopup(POPUP_CONF_BLANK_600_400, 'addChildForm2');
}

function openExercisesModal() {
    $("#addExercisesModal").modal("show");
}

function displayExercisesModalContent(allExercises) {
    console.log("tere", allExercises);
    let addExercisesHtml = ``;
    for (let i = 0; i < allExercises.length; i++) {
        addExercisesHtml = addExercisesHtml + `
        <div>harjutus${allExercises[i].exerciseName}</div>
        `
    }
    document.querySelector('#addExercises').innerHTML = addExercisesHtml;
}

function openSkillsModal(childId) {
    $("#addChildSkillsModal").modal("show");
    displaySkillsModalContent(childId);
}


function displaySkillsModalContent(childId) {
    activeChildId = childId;
    let activeChild = allChildrenItems.find(i => i.id == activeChildId);
    let activeAge = activeChild.prematurityAgeInWeeks;
    let selectedChildSkillGroups = [];
    if (activeAge < 13) {
        selectedChildSkillGroups = skillGroupItems.filter(s => s.ageGroup == "00-03");
    } else if (activeAge < 26) {
        selectedChildSkillGroups = skillGroupItems.filter(s => s.ageGroup == "00-03" || s.ageGroup == "04-06");
    } else if (activeAge < 39) {
        selectedChildSkillGroups = skillGroupItems.filter(s => s.ageGroup == "04-06" || s.ageGroup == "07-09");
    } else if (activeAge < 65) {
        selectedChildSkillGroups = skillGroupItems.filter(s => s.ageGroup == "07-09" || s.ageGroup == "10-12");
    }

    let addChildSkillsHtml = ``;
    for (let i = 0; i < selectedChildSkillGroups.length; i++) {
        console.log(activeChild.childSkills);
        console.log(selectedChildSkillGroups[i].id, "kas lapsel on", activeChild.childSkills.some(cs => cs.id == selectedChildSkillGroups[i].id));
        let childHasCurrentSkill = activeChild.childSkills.some(cs => cs.id == selectedChildSkillGroups[i].id);


        addChildSkillsHtml = addChildSkillsHtml + `
        <div><ul><li>${activeChildId, selectedChildSkillGroups[i].omandatudOskus}</li></ul></div>
        ${
            !childHasCurrentSkill ?
                `<button class="btn btn-secondary" onclick="doSaveSkill(${activeChildId}, ${selectedChildSkillGroups[i].id})">Jah</button>` :
                `<button class="btn btn-secondary" onclick="doRemoveSkill(${activeChildId}, ${selectedChildSkillGroups[i].id})">Ei</button>`
            }
            <button class="btn btn-secondary" onclick="openTextDescription('${selectedChildSkillGroups[i].textDescription}')"><i class="fa fa-question-circle"></i>
            </button>
        `;
    }

    document.querySelector('#addSkills').innerHTML = addChildSkillsHtml;
}

function openTextDescription(selectedSkills) {
    $("#textDescriptionModal").modal("show");
    document.querySelector('#textDescriptionContent').innerHTML = selectedSkills;
}


function displayAllChildrenItems(allChildrenItems) {
    let allChildrenHtml = ``;
    for (let i = 0; i < allChildrenItems.length; i++) {
        let childPicture = allChildrenItems[i].picture;
        if (!childPicture || childPicture == "") {
            childPicture = "images/default1.png";
        }
        allChildrenHtml = allChildrenHtml +
            `
            <div class= "media2">
            <div class="container2">
            <div class="row align-items-start">
                    <div class="col-4">
                        <img  id="childPic"  src="${childPicture}" class="align-self-start mr-3">
                        <div  class="lapse-info">
                            <div>Sünniaeg: ${allChildrenItems[i].age} <span id="childAgeSpan"></span></div>
                            <div>Vanus: ${allChildrenItems[i].trueAgeInWeeks} <span id="childAgeInWeeksSpan"> nädalat </span></div>


                         </div>
                    </div>
                    <div class="col-4">
                        <div class="name"><span id="childFirstNameSpan"><h1>${allChildrenItems[i].firstName.toUpperCase()}</h1> </span></div>
                        <div class="heading-underline"></div>
                    </div> 
                    <div class="col-4">
                    <div class= "heading1">OMANDATUD OSKUSED:</div>
                        <div class= "oskused">${displayChildSkills(allChildrenItems[i].childSkills)}<span id="childSkills"></span></div>
                    
                    </div>
                    </div>
            </div >
            <div class="btn-group" role="group" aria-label="Basic example">
            <button type="button" class="btn btn-secondary" onclick="openChildEditModal(${allChildrenItems[i].id})">Muuda</button>
            <button type="button" class="btn btn-secondary" onclick="doDeleteChild(${allChildrenItems[i].id})">Kustuta</button>
            <button type="button" class="btn btn-secondary" onclick="openSkillsModal(${allChildrenItems[i].id})">Lisa oskused</button>
            <button type="button" class="btn btn-secondary" onclick="doLoadExercises(${allChildrenItems[i].id})">Tee harjutusi</button>
          </div>
            </div >
        `;
    }

    document.querySelector('#allChildren').innerHTML = allChildrenHtml;
}



function displayChildSkills(childSkills) {
    let childSkillsItemsHtml = "";
    for (let j = 0; j < childSkills.length; j++) {
        for (let k = 0; k < childSkills[j].skills.length; k++) {
            childSkillsItemsHtml = childSkillsItemsHtml + `
                <div><i class="fa fa-star-o" aria-hidden="true"> </i>   ${childSkills[j].skills[k].skillName} <span id="childSkills"></span></div>
            `;
        }
    }
    return childSkillsItemsHtml;
}

async function doDeleteChild(id) {
    if (confirm('Soovid kindlasti kustutada?')) {
        await deleteChild(id);
        doLoadChildren();
    }
}


// /*  
//     --------------------------------------------
//     VALIDATION FUNCTIONS
//     --------------------------------------------
// */

function validateChild(child) {
    let errors = [];

    if (child.firstName.length < 2) {
        errors.push('Ebakorrektne nimi (nõutud vähemalt kaks tähemärki)!');
    } else if (!child.age) {
        errors.push('Sisesta sünnikuupäev!')
    }

    // displayErrorBox(errors);
    console.log(errors, child);
    return errors == 0;
}

// ---------------------------------------------page 2-----------------------------


async function displayAllChildrenItems2() {
    let allChildren2Html = ``;
    for (let i = 0; i < allChildrenItems.length; i++) {
        //     let skillCount = 0;
        //     if((allChildrenItems[i].prematurityAgeInWeeks)/4 < 4) {
        //         skillCount= 5;
        //     } else if((allChildrenItems[i].prematurityAgeInWeeks)/4 < 7) {
        //         skillCount= 9;
        //     } else if((allChildrenItems[i].prematurityAgeInWeeks)/4 < 10) {
        //         skillCount= 10;  
        //     } else if((allChildrenItems[i].prematurityAgeInWeeks)/4 < 19) {
        //         skillCount= 11;  
        allChildren2Html = allChildren2Html +
            `
            <div class= "media2">
            <div class="container2">
            <div class="row align-items-start" style="padding:20px">
                    <div class="col-4">
                        <div class="name"><span id="childFirstNameSpan"><h1>${allChildrenItems[i].firstName.toUpperCase()}</h1> </span></div>
                        <div class="heading-underline"></div>
                        
                    </div> 
                    <div class="col-4">
                        <div class="heading2">OMANDATUD OSKUSED:</div><div>${displayChildSkills(allChildrenItems[i].childSkills)}<span id="childSkills"></span></div>
                    </div>
                    <div class="col-4">
                        <div class="heading2">SOORITATUD HARJUTUSED:</div><div>${await displayChildExerciseCount(allChildrenItems[i].exerciseCount, allChildrenItems[i].id)} <span id="exCount"></span></div>
                    </div>
                    <div class="col-12">
                    <div class="heading2">OMANDATUD OSKUSED: </div><div class="heading3">${allChildrenItems[i].childSkills.length}</div>
                    </div>
                    </div>
            </div>
            </div>
        `;
    }
    // }

    document.querySelector('#allChildrenDevelopment').innerHTML = allChildren2Html;
}

async function displayChildExerciseCount(exerciseCount, childId) {
    let childExerciseCountItemsHtml = "";
    let child = allChildrenItems.find(a => a.id == childId);
    console.log("tere", child);
    let allExercises2 = [];
    allExercises2 = await fetchAllExercises(parseInt(Math.ceil(child.prematurityAgeInWeeks / 4)));
    console.log(allExercises2);

    for (let j = 0; j < exerciseCount.length; j++) {
        let currentExerciseName = allExercises2.find(a => a.id == exerciseCount[j].exercise_id);
        console.log(currentExerciseName.exerciseName);
        console.log(exerciseCount[j].count);
        childExerciseCountItemsHtml = childExerciseCountItemsHtml + `
                <div><li>${currentExerciseName.exerciseName} (${exerciseCount[j].count} korda)<span id="exCount"></span></li></div>
               
            `;
    }
    return childExerciseCountItemsHtml;
}

// ------------------------------page3---------------------------


// async function doLoadItems() {
//     try {
//         const items = await fetchCompanies();
//         displayItems(items);
//         displayWrapper();
//     } catch (e) {
//         handleError(e, displayLoginPopup);
//     }
// }



//         if (validateItem(item)) {
//             // Saving the company...
//             await postCompany(item);
//             closePopup();
//             doLoadItems();
//         }
//     } catch (e) {
//         handleError(e, displayLoginPopup);
//     }
// }

// /* 
//     --------------------------------------------
//     CHART FUNCTIONS
//     --------------------------------------------
// */


// function composeChartDataset(items) {
//     if (items != null && items.length > 0) {
//         let data = items.map(item => Math.round(item.marketCapitalization));
//         let backgroundColors = items.map(generateRandomColor);
//         return [{
//             data: data,
//             backgroundColor: backgroundColors,
//         }];
//     }
//     return [];
// }

// function composeChartLabels(items) {
//     return items != null && items.length > 0 ? items.map(item => item.name) : [];
// }

// function composeChartData(companies) {
//     return {
//         datasets: composeChartDataset(companies),
//         labels: composeChartLabels(companies)
//     };
// }


// async function doUploadFile() {
//     const logoFile = document.querySelector('#file').files[0];
//     if (logoFile) {
//         let uploadResponse = await postFile(logoFile);
//         document.querySelector('#itemEditLogoImage').src = uploadResponse.url;
//         document.querySelector('#itemEditLogo').value = uploadResponse.url;
//     }
// }

// async function doLoadChart() {
//     try {
//         const items = await fetchCompanies();
//         displayChart(items);
//         displayWrapper();
//     } catch (e) {
//         handleError(e, displayLoginPopup);
//     }
// }
//
// function displaySkillGroupItems(skillGroupItems) {
//     let skillGroupItemsHtml = /*html*/`
//     //     <div class="items">
//     //         <div class="item-fluid">
//     //             <div class="item-100-center">
//     //                 <h1>Oskused</h1>
//     //             </div>
//     //         </div>
//     //         <div class="item-fluid">
//     //             <div class="item-100-center">
//     //                 <button id="displayModeButton" onclick="doLoadChart()">Näita diagrammi</button>
//     //             </div>
//     //         </div>
//     //     </div>
//     //     <section class="items">
//     // `;
//     for (let i = 0; i < skillGroupItems.length; i++) {
//         skillGroupItemsHtml = skillGroupItemsHtml + displaySkillGroupItems(items[i]);
//     }
// itemsHtml = itemsHtml + /*html*/`
//     </section>
//     <div class="items">
//         <div class="item-fluid">
//             <div class="item-100-center">
//                 <button onclick="displayItemEditPopup()">Lisa ettevõte</button>
//             </div>
//         </div>
//     </div>
// `;
//     document.querySelector('#oskused').innerHTML = skillGroupItemsHtml;
// }

// function displayLoginPopup() {
//     document.querySelector('#wrapper').style.display = 'none';
//     openPopup(POPUP_CONF_BLANK_300_300, 'loginFormTemplate');
// }

// function displaySessionBox() {
//     const sessionBox = document.querySelector('header .header-cell-right');
//     if (!isEmpty(getToken())) {
//         sessionBox.innerHTML = /*html*/`<strong>${getUsername()}</strong> | <a href="javascript:doLogout()">logi välja</a>`;
//     }
// }

// async function displayItemEditPopup(id) {
//     await openPopup(POPUP_CONF_DEFAULT, 'itemEditFormTemplate');

//     // Clearing the possible previous errors...
//     displayErrorBox([]);

//     // Clearing the company edit form from previous data...
//     document.querySelector('#itemEditId').value = '';
//     document.querySelector('#itemEditName').value = '';
//     document.querySelector('#itemEditLogoImage').value = '';
//     document.querySelector('#itemEditLogo').value = '';
//     document.querySelector('#itemEditEstablished').value = '';
//     document.querySelector('#itemEditEmployees').value = '';
//     document.querySelector('#itemEditRevenue').value = '';
//     document.querySelector('#itemEditNetIncome').value = '';
//     document.querySelector('#itemEditSecurities').value = '';
//     document.querySelector('#itemEditSecurityPrice').value = '';
//     document.querySelector('#itemEditDividends').value = '';

//     // Loading the company, if id specified...
//     if (id > 0) {
//         let item = null;
//         try {
//             item = await fetchCompany(id);
//         } catch (e) {
//             handleError(e, displayLoginPopup);
//         }

//         // Filling the company edit form...
//         document.querySelector('#itemEditId').value = item.id;
//         document.querySelector('#itemEditName').value = item.name;
//         document.querySelector('#itemEditLogoImage').src = item.logo;
//         document.querySelector('#itemEditLogo').value = item.logo;
//         document.querySelector('#itemEditEstablished').value = item.established;
//         document.querySelector('#itemEditEmployees').value = item.employees;
//         document.querySelector('#itemEditRevenue').value = item.revenue;
//         document.querySelector('#itemEditNetIncome').value = item.netIncome;
//         document.querySelector('#itemEditSecurities').value = item.securities;
//         document.querySelector('#itemEditSecurityPrice').value = item.securityPrice;
//         document.querySelector('#itemEditDividends').value = item.dividends;
//     }
// }

// function displayWrapper() {
//     document.querySelector('#wrapper').style.display = 'grid';
// }

// function displayItems(items) {
//     let itemsHtml = /*html*/`
//         <div class="items">
//             <div class="item-fluid">
//                 <div class="item-100-center">
//                     <h1>BeebiApp</h1>
//                 </div>
//             </div>
//             <div class="item-fluid">
//                 <div class="item-100-center">
//                     <button id="displayModeButton" onclick="doLoadChart()">Näita diagrammi</button>
//                 </div>
//             </div>
//         </div>
//         <section class="items">
//     `;
//     for (let i = 0; i < items.length; i++) {
//         itemsHtml = itemsHtml + displayItem(items[i]);
//     }
//     itemsHtml = itemsHtml + /*html*/`
//         </section>
//         <div class="items">
//             <div class="item-fluid">
//                 <div class="item-100-center">
//                     <button onclick="displayItemEditPopup()">Lisa ettevõte</button>
//                 </div>
//             </div>
//         </div>
//     `;
//     document.querySelector('main').innerHTML = itemsHtml;
// }

// function displayItem(item) {
//     let itemHtml = /*html*/`
//             <article class="item">
//                 <h2 class="item-100">${item.name}</h2>
//                 <div class="item-100"><img class="item-logo" src="${item.logo}"></div>
//                 <div class="item-50-bold">Asutatud</div>
//                 <div class="item-50">${item.established}</div>
//                 <div class="item-50-bold">Töötajaid</div>
//                 <div class="item-50">${formatNumber(item.employees)}</div>
//                 <div class="item-50-bold">Müügitulu</div>
//                 <div class="item-50">${formatNumber(item.revenue)} &euro;</div>
//                 <div class="item-50-bold">Netotulu</div>
//                 <div class="item-50">${formatNumber(item.netIncome)} &euro;</div>
//                 <div class="item-50-bold">Turuväärtus</div>
//                 <div class="item-50">${formatNumber(parseInt(item.marketCapitalization))} &euro;</div>
//                 <div class="item-50-bold">Dividendimäär</div>
//                 <div class="item-50">${formatNumber(item.dividendYield * 100)} %</div>
//                 <div class="item-100-center">
//                     <button onclick="displayItemEditPopup(${item.id})">Muuda</button> 
//                     <button onclick="doDeleteItem(${item.id})" class="button-red">Kustuta</button>
//                 </div>
//             </article>
//         `;
//     return itemHtml;
// }

// function displayErrorBox(errors) {
//     const itemEditFormContainer = document.querySelector('#itemEditFormContainer');
//     const errorBox = document.querySelector('#errorBox');
//     if (errors.length > 0) {
//         errorBox.style.display = 'block';

//         let errorsHtml = /*html*/`<div><strong>Palun paranda järgmised vead:</strong></div>`;
//         for (let i = 0; i < errors.length; i++) {
//             errorsHtml = errorsHtml + /*html*/`
//                 <div>
//                     ${errors[i]}
//                 </div>
//             `;
//         }

//         errorBox.innerHTML = errorsHtml;
//         itemEditFormContainer.scrollTop = 0;
//     } else {
//         errorBox.innerHTML = '';
//     }
// }

// function displayChart(items) {
//     let chartHtml = /*html*/`
//         <div class="items">
//             <div class="item-fluid">
//                 <div class="item-100-center">
//                     <h1>Börsiettevõtted</h1>
//                 </div>
//             </div>
//             <div class="item-fluid">
//                 <div class="item-100-center">
//                     <button id="displayModeButton" onclick="doLoadItems()">Näita nimekirja</button>
//                 </div>
//             </div>
//             <div class="item-fluid">
//                 <canvas id="chartCanvas" style="height:40vh; width:80%"></canvas>
//             </div>
//         </div>
//     `;
//     document.querySelector('main').innerHTML = chartHtml;

//     let ctx = document.querySelector('#chartCanvas').getContext('2d');
//     let chartData = composeChartData(items);
//     new Chart(ctx, {
//         type: 'pie',
//         data: chartData,
//         options: {}
//     });
//}
