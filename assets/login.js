
async function doLogin() {
    try {
        const credentials = {
            username: document.querySelector('#loginUsername').value,
            password: document.querySelector('#loginPassword').value,
        };
        const session = await login(credentials);
        storeAuthentication(session);
        document.location = "./userindex.html#page1";
    } catch (e) {
        console.log(e);
    }
}

async function doRegister() {
    try {
        const credentials = {
            username: document.querySelector('#registerUsername').value,
            password: document.querySelector('#registerPassword').value,
        };
        const response = await register(credentials);
        if (response.errors.length > 0) {
           // vead serverist
        } else {
           document.querySelector('#registrationContainer').style.display="none";
           document.querySelector('#registrationSuccessContainer').style.display="block";
           document.querySelector('#registerButton').style.display = "none";
        }
    } catch (e) {
        console.log(e);
    }
}

function doLogout(){
    clearAuthentication();
    document.querySelector('#allChildren').innerHTML=" ";
    document.location = "./loginindex.html";
}



function openRegisterModal() {
    $('#loginModal').modal('hide');
    $('#registerModal').modal('show');
}

function openLoginModal() {
    $('#loginModal').modal('show');
    $('#registerModal').modal('hide');
}

document.addEventListener("keyup", function (event){
    if(13=== event.keyCode){
        let usernameInput= document.querySelector("#loginUsername").value;
        let passwordInput= document.querySelector("#loginPassword").value;
        if(!usernameInput|| usernameInput== " "|| !passwordInput || passwordInput== " "){
            return false;
        }
        doLogin();
    }

}

);